# dwmblocks
Modular status bar for dwm written in c.

## Modifying blocks
The statusbar is made from text output from commandline programs. Blocks are added and removed by editing the config.h header file.

Block scripts are located in [my dotfile's repo.](https://gitlab.com/minguyen/dotfiles/-/tree/master/.local/bin/statusbar)

## Clickable modules
Like i3blocks, this build allows you to build in additional actions into your
scripts in response to click events. See the above linked scripts for examples
of this using the `$BLOCK_BUTTON` variable.
